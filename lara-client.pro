QT += qml quick svg

CONFIG += c++11

SOURCES += main.cpp

lupdate_only{
SOURCES = common/*.qml \
          common/*.js \
          pages/prodcalc/*.qml \
          pages/prodcalc/*.js \
          pages/records/*.qml \
          pages/records/*.js \
          pages/reports/*.qml \
          pages/reports/*.js \
          *.qml \
          *.js
}

TRANSLATIONS = lara_sk.ts

RESOURCES += qml.qrc

TARGET = lara

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

contains(ANDROID_TARGET_ARCH,armeabi-v7a) {
    ANDROID_EXTRA_LIBS = \
        $$PWD/android_libs/libcrypto.so \
        $$PWD/android_libs/libssl.so
}

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
