import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

import "common"

ApplicationWindow {
    id: mainWindow
    visible: true
    width: 640
    height: 480
    title: "lara"

    property string version

    property string token

    Settings {
        id: settings
        // API
        property string login: "login"
        property string password: "password"
        property string apiURL: "https://localhost:8443"

        //GUI
        property alias windowX: mainWindow.x
        property alias windowY: mainWindow.y
        property alias windowWidth: mainWindow.width
        property alias windowHeight: mainWindow.height
    }

    WorkerScript {
        id: statusWorker
        source: "qrc:/api.js"
        onMessage: {
            connStatus.text = messageObject.statusIcon
        }
    }

    header: ToolBar {
        RowLayout {
            id: toolBarContainer
            anchors.fill: parent

            ToolButton {
                contentItem: Label {
                    horizontalAlignment: Label.AlignHCenter
                    verticalAlignment: Label.AlignVCenter
                    font.family: "Material Icons"
                    font.pixelSize: 24
                    text: "menu"
                }
                onClicked: drawer.open()
            }

            Label {
                id: pageLabel
                text: qsTr("Records")
            }

            Item {
                Layout.fillWidth: true
            }

            SearchField {
                id: searchField
            }

            Item {
                Layout.fillWidth: true
            }

            Label {
                id: connStatus
                horizontalAlignment: Label.AlignHCenter
                verticalAlignment: Label.AlignVCenter
                font.family: "Material Icons"
                font.pixelSize: 24
                text: "signal_cellular_null"
                Timer {
                    id: healthCheckTimer
                    interval: 30000
                    running: true
                    repeat: true
                    triggeredOnStart: true
                    onTriggered: {
                        statusWorker.sendMessage({
                                                     action: "healthcheck",
                                                     apiURL: settings.apiURL
                                                 })
                    }
                }
            }

            ToolButton {
                contentItem: Label {
                    horizontalAlignment: Label.AlignHCenter
                    verticalAlignment: Label.AlignVCenter
                    font.family: "Material Icons"
                    font.pixelSize: 24
                    text: "more_vert"
                }

                onClicked: optionsMenu.open()

                Menu {
                    id: optionsMenu
                    x: parent.width - width
                    transformOrigin: Menu.TopRight

                    MenuItem {
                        text: qsTr("Settings")
                        onTriggered: settingsDialog.open()
                    }

                    MenuItem {
                        text: qsTr("About")
                        onTriggered: aboutDialog.open()
                    }
                }
            }
        }
    }

    Drawer {
        id: drawer
        width: 0.66 * mainWindow.width
        height: mainWindow.height

        ListView {
            id: navList

            focus: true
            currentIndex: -1
            anchors.fill: parent

            topMargin: 16
            bottomMargin: 16

            delegate: ItemDelegate {
                width: navList.width
                leftPadding: menuIcon.implicitWidth + 32

                text: model.title
                highlighted: ListView.isCurrentItem
                onClicked: {
                    navList.currentIndex = index
                    pageLoader.source = model.source
                    pageLabel.text = model.title
                    searchField.clear()
                    searchField.visible = model.searchActive
                    drawer.close()
                }

                Label {
                    id: menuIcon
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: 16
                    font.family: "Material Icons"
                    font.pixelSize: 24
                    text: model.icon
                }
            }

            model: ListModel {
                ListElement {
                    title: qsTr("Products")
                    source: "qrc:/pages/prodcalc/ProductCalculator.qml"
                    icon: "format_list_numbered"
                    searchActive: true
                }
                ListElement {
                    title: qsTr("Records")
                    source: "qrc:/pages/records/RecordsStack.qml"
                    icon: "pets"
                    searchActive: true
                }
                ListElement {
                    title: qsTr("Reports")
                    source: "qrc:/pages/reports/Reports.qml"
                    icon: "euro_symbol"
                    searchActive: false
                }
            }

            ScrollIndicator.vertical: ScrollIndicator {
            }
        }
    }

    Loader {
        id: pageLoader
        anchors.fill: parent
        source: "qrc:/pages/records/RecordsStack.qml"
    }

    Item {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        ToolTip {
            id: statusToolTip
            timeout: 4000
            visible: false
            text: ''
        }
    }

    SettingsDialog {
        id: settingsDialog
    }
    AboutDialog {
        id: aboutDialog
    }
}
