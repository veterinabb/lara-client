<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sk_SK" sourcelanguage="en_US">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="common/AboutDialog.qml" line="7"/>
        <source>About</source>
        <translation>O aplikácii</translation>
    </message>
    <message>
        <location filename="common/AboutDialog.qml" line="19"/>
        <source>Lara client version %1</source>
        <translation>Lara klientská aplikácia verzia %1</translation>
    </message>
</context>
<context>
    <name>OwnerDetail</name>
    <message>
        <location filename="pages/records/OwnerDetail.qml" line="84"/>
        <source>Name:</source>
        <translation>Meno:</translation>
    </message>
    <message>
        <location filename="pages/records/OwnerDetail.qml" line="98"/>
        <source>Address:</source>
        <translation>Adresa:</translation>
    </message>
    <message>
        <location filename="pages/records/OwnerDetail.qml" line="112"/>
        <source>Phone 1:</source>
        <translation>Tel. 1:</translation>
    </message>
    <message>
        <location filename="pages/records/OwnerDetail.qml" line="126"/>
        <source>Phone 2:</source>
        <translation>Tel. 2:</translation>
    </message>
    <message>
        <location filename="pages/records/OwnerDetail.qml" line="140"/>
        <source>Email:</source>
        <translation>Email:</translation>
    </message>
    <message>
        <location filename="pages/records/OwnerDetail.qml" line="154"/>
        <source>IC:</source>
        <translation>IČ:</translation>
    </message>
    <message>
        <location filename="pages/records/OwnerDetail.qml" line="168"/>
        <source>DIC:</source>
        <translation>DIČ:</translation>
    </message>
    <message>
        <location filename="pages/records/OwnerDetail.qml" line="182"/>
        <source>ICDPH:</source>
        <translation>IČDPH:</translation>
    </message>
    <message>
        <location filename="pages/records/OwnerDetail.qml" line="196"/>
        <source>Note:</source>
        <translation>Poznámka:</translation>
    </message>
    <message>
        <location filename="pages/records/OwnerDetail.qml" line="210"/>
        <source>Created:</source>
        <translation>Vytvoril:</translation>
    </message>
    <message>
        <location filename="pages/records/OwnerDetail.qml" line="224"/>
        <source>Last modified:</source>
        <translation>Naposledy opravil:</translation>
    </message>
    <message>
        <location filename="pages/records/OwnerDetail.qml" line="238"/>
        <source>Back</source>
        <translation>Späť</translation>
    </message>
    <message>
        <location filename="pages/records/OwnerDetail.qml" line="243"/>
        <source>Edit</source>
        <translation>Opraviť</translation>
    </message>
</context>
<context>
    <name>PatientDetail</name>
    <message>
        <location filename="pages/records/PatientDetail.qml" line="75"/>
        <source>Name:</source>
        <translation>Meno:</translation>
    </message>
    <message>
        <location filename="pages/records/PatientDetail.qml" line="89"/>
        <source>Birth date:</source>
        <translation>Dátum narodenia:</translation>
    </message>
    <message>
        <location filename="pages/records/PatientDetail.qml" line="103"/>
        <source>Species:</source>
        <translation>Druh:</translation>
    </message>
    <message>
        <location filename="pages/records/PatientDetail.qml" line="117"/>
        <source>Breed:</source>
        <translation>Plemeno:</translation>
    </message>
    <message>
        <location filename="pages/records/PatientDetail.qml" line="131"/>
        <source>Gender:</source>
        <translation>Pohlavie:</translation>
    </message>
    <message>
        <location filename="pages/records/PatientDetail.qml" line="145"/>
        <source>Note:</source>
        <translation>Poznámka:</translation>
    </message>
    <message>
        <location filename="pages/records/PatientDetail.qml" line="159"/>
        <source>Dead:</source>
        <translation>Nežije:</translation>
    </message>
    <message>
        <location filename="pages/records/PatientDetail.qml" line="173"/>
        <source>Created:</source>
        <translation>Vytvoril:</translation>
    </message>
    <message>
        <location filename="pages/records/PatientDetail.qml" line="187"/>
        <source>Last modified:</source>
        <translation>Naposledy opravil:</translation>
    </message>
    <message>
        <location filename="pages/records/PatientDetail.qml" line="201"/>
        <source>Back</source>
        <oldsource>back</oldsource>
        <translation>Späť</translation>
    </message>
    <message>
        <location filename="pages/records/PatientDetail.qml" line="206"/>
        <source>Edit</source>
        <translation>Opraviť</translation>
    </message>
</context>
<context>
    <name>ProductCalculator</name>
    <message>
        <location filename="pages/prodcalc/ProductCalculator.qml" line="37"/>
        <source>More items found</source>
        <translation>Našlo sa viac záznamov</translation>
    </message>
    <message>
        <location filename="pages/prodcalc/ProductCalculator.qml" line="39"/>
        <source>No items</source>
        <translation>Žiadne záznamy</translation>
    </message>
    <message>
        <location filename="pages/prodcalc/ProductCalculator.qml" line="62"/>
        <source>Products (tap for add in)</source>
        <translation>Produkty (ťuk pre započítanie)</translation>
    </message>
    <message>
        <location filename="pages/prodcalc/ProductCalculator.qml" line="97"/>
        <source>Count in (tap for remove)</source>
        <translation>Započítané (ťuk pre odstránenie)</translation>
    </message>
    <message>
        <location filename="pages/prodcalc/ProductCalculator.qml" line="179"/>
        <source>Add in</source>
        <translation>Započítať</translation>
    </message>
    <message>
        <location filename="pages/prodcalc/ProductCalculator.qml" line="225"/>
        <source>Remove item</source>
        <translation>Odstrániť položku</translation>
    </message>
    <message>
        <location filename="pages/prodcalc/ProductCalculator.qml" line="127"/>
        <source>Remove %1?</source>
        <translation>Odstrániť %1?</translation>
    </message>
    <message>
        <location filename="pages/prodcalc/ProductCalculator.qml" line="140"/>
        <source>Total</source>
        <translation>Celkovo</translation>
    </message>
    <message>
        <location filename="pages/prodcalc/ProductCalculator.qml" line="161"/>
        <source>clear</source>
        <translation>vyčistiť</translation>
    </message>
    <message>
        <location filename="pages/prodcalc/ProductCalculator.qml" line="206"/>
        <source>Amount:</source>
        <translation>Množstvo:</translation>
    </message>
</context>
<context>
    <name>RecordDetail</name>
    <message>
        <location filename="pages/records/RecordDetail.qml" line="69"/>
        <source>Record from:</source>
        <translation>Záznam z dňa:</translation>
    </message>
    <message>
        <location filename="pages/records/RecordDetail.qml" line="83"/>
        <source>Enter text</source>
        <translation>Zadajte text</translation>
    </message>
    <message>
        <location filename="pages/records/RecordDetail.qml" line="89"/>
        <source>Billed</source>
        <translation>Účtovaný</translation>
    </message>
    <message>
        <location filename="pages/records/RecordDetail.qml" line="95"/>
        <source>Created:</source>
        <translation>Vytvoril:</translation>
    </message>
    <message>
        <location filename="pages/records/RecordDetail.qml" line="109"/>
        <source>Last modified:</source>
        <translation>Naposledy opravil:</translation>
    </message>
    <message>
        <location filename="pages/records/RecordDetail.qml" line="123"/>
        <source>Back</source>
        <translation>Späť</translation>
    </message>
    <message>
        <location filename="pages/records/RecordDetail.qml" line="157"/>
        <source>Total:</source>
        <translation>Celkom:</translation>
    </message>
</context>
<context>
    <name>RecordSearch</name>
    <message>
        <location filename="pages/records/RecordSearch.qml" line="39"/>
        <source>More items found</source>
        <translation>Našlo sa viac záznamov</translation>
    </message>
    <message>
        <location filename="pages/records/RecordSearch.qml" line="41"/>
        <source>No items</source>
        <translation>Žiadne záznamy</translation>
    </message>
</context>
<context>
    <name>Reports</name>
    <message>
        <location filename="pages/reports/Reports.qml" line="32"/>
        <source>Date from:</source>
        <translation>Dátum od:</translation>
    </message>
    <message>
        <location filename="pages/reports/Reports.qml" line="46"/>
        <source>Date to:</source>
        <translation>Dátum do:</translation>
    </message>
    <message>
        <location filename="pages/reports/Reports.qml" line="62"/>
        <source>run</source>
        <translation>spusti</translation>
    </message>
    <message>
        <location filename="pages/reports/Reports.qml" line="91"/>
        <source>Total records:</source>
        <translation>Počet záznamov:</translation>
    </message>
    <message>
        <location filename="pages/reports/Reports.qml" line="106"/>
        <source>Income:</source>
        <translation>Príjmy:</translation>
    </message>
    <message>
        <location filename="pages/reports/Reports.qml" line="121"/>
        <source>Income (billed):</source>
        <translation>Príjmy (vystavené):</translation>
    </message>
    <message>
        <location filename="pages/reports/Reports.qml" line="136"/>
        <source>Income (not billed):</source>
        <translation>Príjmy (nevystavené):</translation>
    </message>
</context>
<context>
    <name>SearchField</name>
    <message>
        <location filename="common/SearchField.qml" line="5"/>
        <source>Search...</source>
        <translation>Hľadať...</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="common/SettingsDialog.qml" line="11"/>
        <source>Settings</source>
        <translation>Nastavenia</translation>
    </message>
    <message>
        <location filename="common/SettingsDialog.qml" line="36"/>
        <source>Login:</source>
        <translation>Login:</translation>
    </message>
    <message>
        <location filename="common/SettingsDialog.qml" line="50"/>
        <source>Password:</source>
        <translation>Heslo:</translation>
    </message>
    <message>
        <location filename="common/SettingsDialog.qml" line="65"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
</context>
<context>
    <name>guiutil</name>
    <message>
        <location filename="common/guiutil.js" line="4"/>
        <source>Error: %1</source>
        <translation>Chyba: %1</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="main.qml" line="169"/>
        <source>Products</source>
        <translation>Kalkulačka</translation>
    </message>
    <message>
        <location filename="main.qml" line="113"/>
        <source>Settings</source>
        <translation>Nastavenia</translation>
    </message>
    <message>
        <location filename="main.qml" line="118"/>
        <source>About</source>
        <translation>O aplikácii</translation>
    </message>
    <message>
        <location filename="main.qml" line="59"/>
        <location filename="main.qml" line="175"/>
        <source>Records</source>
        <translation>Záznamy</translation>
    </message>
    <message>
        <location filename="main.qml" line="181"/>
        <source>Reports</source>
        <translation>Štatistiky</translation>
    </message>
</context>
</TS>
