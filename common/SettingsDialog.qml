import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3

Dialog {
    x: Math.round((mainWindow.width - width) / 2)
    y: Math.round(mainWindow.height / 6)
    width: Math.round(Math.min(mainWindow.width, mainWindow.height) / 3 * 2)
    modal: true
    focus: true
    title: qsTr("Settings")

    standardButtons: Dialog.Ok | Dialog.Cancel
    onAccepted: {
        settings.login = loginField.text
        settings.password = passwordField.text
        settings.apiURL = apiURLfield.text
        close()
    }
    onRejected: {
        loginField.text = settings.login
        passwordField.text = settings.password
        apiURLfield.text = settings.apiURL
        close()
    }

    contentItem: ColumnLayout {
        id: settingsColumn
        spacing: 24

        // TODO serverCert
        RowLayout {
            spacing: 8

            Label {
                text: qsTr("Login:")
            }

            TextField {
                id: loginField
                text: settings.login
                Layout.fillWidth: true
            }
        }

        RowLayout {
            spacing: 8

            Label {
                text: qsTr("Password:")
            }

            TextField {
                id: passwordField
                echoMode: TextInput.Password
                text: settings.password
                Layout.fillWidth: true
            }
        }

        RowLayout {
            spacing: 8

            Label {
                text: qsTr("URL:")
            }

            TextField {
                id: apiURLfield
                text: settings.apiURL
                Layout.fillWidth: true
            }
        }
    }
}
