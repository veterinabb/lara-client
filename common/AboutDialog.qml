import QtQuick 2.7
import QtQuick.Controls 2.1

Dialog {
    modal: true
    focus: true
    title: qsTr("About")
    x: (mainWindow.width - width) / 2
    y: mainWindow.height / 6
    width: Math.min(mainWindow.width, mainWindow.height) / 3 * 2
    contentHeight: aboutColumn.height

    Column {
        id: aboutColumn
        spacing: 24

        Label {
            width: Dialog.availableWidth
            text: qsTr("Lara client version %1").arg(mainWindow.version)
            wrapMode: Label.Wrap
            font.pixelSize: 12
        }

        Label {
            width: Dialog.availableWidth
            text: "Copyright (C) 2016-2017 Jan Kusniar"
            wrapMode: Label.Wrap
            font.pixelSize: 12
        }

        Text {
            text: '<html><a href="https://gitlab.com/veterinabb/lara-client">Sources on gitlab</a></html>'
            onLinkActivated: Qt.openUrlExternally(link)
            MouseArea {
                anchors.fill: parent
                acceptedButtons: Qt.NoButton
                cursorShape: parent.hoveredLink ? Qt.PointingHandCursor : Qt.ArrowCursor
            }
        }
    }
}
