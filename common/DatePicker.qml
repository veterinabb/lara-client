import QtQuick 2.7
import QtQuick.Controls 2.1

// Custom date picker. Use QtQuick.Controls 2 when available
Button {
    property date date: new Date()
    property bool endOfDay: false

    Component.onCompleted: {
        date = computeTimeOfDay(date, endOfDay)
    }

    function computeTimeOfDay(dt, endOfDay) {
        if (endOfDay) {
            return new Date(dt.getFullYear(), dt.getMonth(), dt.getDate(),
                            23, 59, 59)
        } else {
            return new Date(dt.getFullYear(), dt.getMonth(),
                            dt.getDate(), 0, 0, 0)
        }
    }

    text: date.toLocaleDateString()
    flat: true
    onClicked: {
        cal.selectedDate = date
        datePickerDialog.open()
    }

    Dialog {
        id: datePickerDialog
        modal: true
        focus: true

        standardButtons: Dialog.Ok | Dialog.Cancel
        onAccepted: {
            parent.date = computeTimeOfDay(cal.selectedDate, parent.endOfDay)
            close()
        }
        onRejected: {
            close()
        }

        contentItem: MyCalendar {
            id: cal
            onDoubleClicked: datePickerDialog.accept()
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: datePickerDialog.footer.top
        }
    }
}
