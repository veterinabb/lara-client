import QtQuick 2.7
import QtQuick.Controls 2.1

TextField {
    placeholderText: qsTr("Search...")
    onAccepted: pageLoader.item.search(text)
}
