function updateConnectionStatus(messageObject) {
    if (messageObject.statusCode !== 200) {
        console.log("ERROR: " + messageObject.statusCode + " - " + messageObject.errorMessage)
        statusToolTip.text = qsTr("Error: %1").arg(messageObject.errorMessage)
        statusToolTip.visible = true
        connStatus.text = "signal_cellular_connected_no_internet_4_bar"
        healthCheckTimer.stop()
    } else {
        mainWindow.token = messageObject.newToken
        connStatus.text = "signal_cellular_4_bar"
        if (!healthCheckTimer.running) {
            healthCheckTimer.start()
        }
    }
}

function emptyIfUndefined(obj) {
    if (typeof obj === "undefined") {
        return ""
    }
    return obj
}

function formatModifier(modifier, modified) {
    if (emptyIfUndefined(modifier) !== "") {
        return modifier + " " + new Date(emptyIfUndefined(
                                             modified)).toLocaleString(
                    Qt.locale(), Locale.NarrowFormat)
    }

    return ""
}
