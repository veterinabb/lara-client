import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.1

ColumnLayout {
    property alias placeholderText: inputField.placeholderText
    default property alias text: inputField.text
    property alias acceptableInput: inputField.acceptableInput

    // input validation
    property int minChars: 0
    property int maxChars: 1024

    // descriptive label
    property string label: ""


    // TODO fix component dimensions & placement
    // according to https://material.io/guidelines/components/text-fields.html#text-fields-input
    Label {
        text: label
        color: inputField.activeFocus ? Material.accent : Material.color(
                                          Material.Grey, Material.Shade400)
    }
    TextField {
        id: inputField
        font.pixelSize: 16
        validator: RegExpValidator {
            regExp: new RegExp("^.{" + minChars + "," + maxChars + "}$")
        }
    }
    Label {
        text: inputField.length + "/" + maxChars
        color: inputField.acceptableInput ? Material.color(
                                              Material.Grey,
                                              Material.Shade400) : Material.accent
    }
}
