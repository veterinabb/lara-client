import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.1

import "guiutil.js" as Guiutil

ColumnLayout {
    property alias placeholderText: inputField.placeholderText

    // displayed text
    default property alias text: inputField.text

    // ID of selected item
    property int selectedId: 0

    // descriptive label
    property string label: ""

    // API search action name
    property string searchAction: "DefaultAction"

    // API additional properties JSON
    property var apiParams: ({

                             })

    // emitted on item selected
    signal selected (int selId)

    WorkerScript {
        id: worker
        source: "qrc:/api.js"
        onMessage: {
            Guiutil.updateConnectionStatus(messageObject)
            if (messageObject.statusCode === 200
                    && messageObject.action === searchAction) {
                searchList.visible = true
            }
        }
    }

    Label {
        text: label
        color: inputField.activeFocus ? Material.accent : Material.color(
                                            Material.Grey, Material.Shade400)
    }

    TextField {
        id: inputField
        placeholderText: qsTr("Search")
        font.pixelSize: 16
        // select all on focus
        onFocusChanged: {
            if (focus) {
                selectAll()
            }
        }
    }

    // search while typing
    Keys.onReleased: {
        var apiRequest = {
            action: searchAction,
            query: inputField.text,
            model: searchModel,
            login: settings.login,
            pass: settings.password,
            apiURL: settings.apiURL,
            token: mainWindow.token
        }

        for (var key in apiParams) {
            if (apiParams.hasOwnProperty(key)) {
                apiRequest[key] = apiParams[key]
            }
        }

        worker.sendMessage(apiRequest)
        event.accepted = true
    }

    ListView {
        id: searchList
        visible: false
        anchors.top: inputField.bottom
        width: inputField.width
        height: 120

        model: ListModel {
            id: searchModel
        }

        delegate: ItemDelegate {
            text: name
            width: parent.width
            onClicked: {
                inputField.text = name
                selectedId = id
                searchList.visible = false
                selected(id)
            }
        }

        ScrollIndicator.vertical: ScrollIndicator {
        }
    }
}
