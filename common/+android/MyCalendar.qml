import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

// TODO use QtQuick.Controls 2 only
Calendar {
    style: CalendarStyle {
        navigationBar: Rectangle {
            color: "#f9f9f9"
            height: dateText.height * 2

            Rectangle {
                color: Qt.rgba(1, 1, 1, 0.6)
                height: 1
                width: parent.width
            }

            Rectangle {
                anchors.bottom: parent.bottom
                height: 1
                width: parent.width
                color: "#ddd"
            }
            ToolButton {
                id: previousMonth
                height: parent.height
                width: height
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left

                Label {
                    font.family: "Material Icons"
                    font.pixelSize: 24
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    fontSizeMode: Text.Fit
                    anchors.fill: parent
                    text: "navigate_before"
                }

                onClicked: control.showPreviousMonth()
            }
            Label {
                id: dateText
                text: styleData.title
                font.pixelSize: 16
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                fontSizeMode: Text.Fit
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: previousMonth.right
                anchors.leftMargin: 2
                anchors.right: nextMonth.left
                anchors.rightMargin: 2
            }
            ToolButton {
                id: nextMonth
                height: parent.height
                width: height
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right

                Label {
                    font.family: "Material Icons"
                    font.pixelSize: 24
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    fontSizeMode: Text.Fit
                    anchors.fill: parent
                    text: "navigate_next"
                }
                onClicked: control.showNextMonth()
            }
        }
    }
}
