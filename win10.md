# Windows 10 Development Setup Notes

## Installation

To enable MSVC 64bit builds, install following software:

* CMake
  * Do not add CMake to PATH
* OpenSSL 1.0.2L (https://slproweb.com/products/Win32OpenSSL.html)
  * Choose "Copy OpenSSL DLLs" to "The Windows System directory"
* Visual Studio Community 
  * check only "Desktop development with C++"
* Debugging Tools for Windows
  * https://developer.microsoft.com/en-us/windows/hardware/download-windbg
  * download Windows SDK and uncheck everything except debugging tools in installer
* QT (unified online installer)
  * in setup check also:
    * Qt -> msvc2017 64bit 
    * Qt -> sources

## QTCreator Configuration
In QtCreator configure:

* add Cmake (Options -> Build&Run -> CMake)
* add CDB path according to (http://doc.qt.io/qtcreator/creator-debugger-engines.html#setting-cdb-paths-on-windows)

## Application Configuration

* run qt linguist prior to first compilation
* copy correct servers' cert to projects' folder
* application settings are stored in registry (HKEY_CURRENT_USER\Software\veterinabb\lara)

# Windows 10 Deployment

* Use windeployqt:
  * create deployment directory containing released exe file 
  * Run "Qt XXX 64 bit for Desktop (MSVC 2017)" cmd from start menu
  * `cd <DEPLOY_DIR>`
  * `windeployqt --qmldir <PROJECT_SOURCE_DIR> .`
  * copy cert.pem to <DEPLOY_DIR>
  * copy openssl dlls to <DEPLOY_DIR>

