import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.1

import "../../common/guiutil.js" as Guiutil

Pane {
    signal search(string text)
    signal loadOwner(int oid)

    Component.onCompleted: {
        search.connect(startSearch)
    }

    function startSearch(text) {
        recordsModel.searchRunning = true
        worker.sendMessage({
                               action: "search",
                               model: recordsModel,
                               request: {
                                   query: text
                               },
                               login: settings.login,
                               pass: settings.password,
                               apiURL: settings.apiURL,
                               token: mainWindow.token
                           })
    }

    WorkerScript {
        id: worker
        source: "qrc:/api.js"
        onMessage: {
            recordsModel.searchRunning = false
            Guiutil.updateConnectionStatus(messageObject)
            if (messageObject.statusCode === 200) {
                if (messageObject.moreItems) {
                    statusToolTip.text = qsTr("More items found")
                } else if (messageObject.total === 0) {
                    statusToolTip.text = qsTr("No items")
                }
                statusToolTip.visible = messageObject.moreItems
                        || messageObject.total === 0
            }
        }
    }

    BusyIndicator {
        id: searchIndicator
        running: recordsModel.searchRunning
        anchors.centerIn: parent
    }
    ListView {
        id: records
        anchors.fill: parent

        ListModel {
            id: recordsModel
            property bool searchRunning: false
        }
        model: recordsModel

        delegate: ItemDelegate {
            id: recDlg
            width: parent.width
            height: 72 // MD spec: 72dp

            contentItem: Item {
                Label {
                    anchors.left: parent.left
                    //anchors.leftMargin: 0 // MD spec: 16dp
                    anchors.verticalCenter: parent.verticalCenter
                    font.family: "Material Icons"
                    font.pixelSize: 48
                    text: "account_box"
                }

                ColumnLayout {
                    anchors.left: parent.left
                    anchors.leftMargin: 72 // MD spec: 16dp + 72dp
                    anchors.verticalCenter: parent.verticalCenter

                    Label {
                        text: model.name
                        font.bold: true
                        font.pixelSize: 16
                    }

                    Label {
                        text: model.address
                    }
                }
            }

            /* content frame - better for small screen
            background: Rectangle {
                implicitWidth: 100
                implicitHeight: 40
                color: recDlg.hovered ? Material.color(
                                            Material.Grey,
                                            Material.Shade200) : Material.background
                Rectangle {
                    width: parent.width
                    height: 1
                    color: Material.color(Material.Grey, Material.Shade300)
                    anchors.bottom: parent.bottom
                }
            }*/

            onClicked: {
                loadOwner(model.id)
            }
        }
    }
}
