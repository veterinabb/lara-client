import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.1

import "../../common/guiutil.js" as Guiutil

Pane {
    property bool isLoading: false
    property int recordId: -1
    property int version: -1

    signal back
    signal saved

    function load(recordId) {
        isLoading = true
        worker.sendMessage({
                               action: "getRecord",
                               rid: recordId,
                               model: itemsModel,
                               login: settings.login,
                               pass: settings.password,
                               apiURL: settings.apiURL,
                               token: mainWindow.token
                           })
    }

    Component.onCompleted: {
        forceActiveFocus() // Required for android back button to work!
    }

    WorkerScript {
        id: worker
        source: "qrc:/api.js"
        onMessage: {
            isLoading = false
            Guiutil.updateConnectionStatus(messageObject)
            if (messageObject.statusCode === 200) {

                if (messageObject.action === "getRecord") {
                    saveBtn.enabled = true
                    recordId = messageObject.record.id
                    version = messageObject.record.version
                    recText.text = messageObject.record.text
                    recDate.text = new Date(messageObject.record.date).toLocaleString(
                                Qt.locale(), Locale.ShortFormat)
                    recBilled.checked = messageObject.record.billed
                    total.text = Number(
                                messageObject.record.total).toLocaleCurrencyString(
                                Qt.locale())
                    creator.text = messageObject.record.creator
                    modifier.text = Guiutil.formatModifier(
                                messageObject.record.modifier,
                                messageObject.record.modified)
                } else if (messageObject.action === "updateRecord") {
                    saved()
                }
            }
        }
    }

    ColumnLayout {
        anchors.fill: parent

        Pane {
            Material.elevation: 4
            Layout.fillWidth: true

            GridLayout {
                flow: mainWindow.width
                      > mainWindow.height ? GridLayout.LeftToRight : GridLayout.TopToBottom
                columns: 2 // used only when GridLayout.LeftToRight
                anchors.fill: parent

                RowLayout {
                    Label {
                        text: qsTr("Record from:")
                        Layout.alignment: Qt.AlignBottom
                    }
                    Label {
                        id: recDate
                        Layout.alignment: Qt.AlignBottom
                        text: ""
                        font.pixelSize: 16
                        font.bold: true
                    }
                }

                TextArea {
                    id: recText
                    placeholderText: qsTr("Enter text")
                    wrapMode: TextArea.Wrap
                }

                CheckBox {
                    id: recBilled
                    text: qsTr("Billed")
                    enabled: false
                }

                RowLayout {
                    Label {
                        text: qsTr("Created:")
                        Layout.alignment: Qt.AlignBottom
                    }
                    Label {
                        id: creator
                        Layout.alignment: Qt.AlignBottom
                        text: ""
                        font.pixelSize: 16
                        font.bold: true
                    }
                }

                RowLayout {
                    Label {
                        text: qsTr("Last modified:")
                        Layout.alignment: Qt.AlignBottom
                    }
                    Label {
                        id: modifier
                        Layout.alignment: Qt.AlignBottom
                        text: ""
                        font.pixelSize: 16
                        font.bold: true
                    }
                }

                RowLayout {
                    Button {
                        text: qsTr("Back")
                        highlighted: true
                        onClicked: back()
                    }

                    Button {
                        id: saveBtn
                        text: qsTr("Save")
                        enabled: false
                        highlighted: true
                        onClicked: {
                            isLoading = true
                            var items = []
                            for (var i = 0; i < itemsModel.count; i++) {
                                items.push({
                                               productId: itemsModel.get(
                                                              i).productId,
                                               productPrice: itemsModel.get(
                                                                 i).productPrice,
                                               amount: itemsModel.get(
                                                                 i).amount,
                                               itemPrice: itemsModel.get(
                                                                 i).itemPrice,
                                               itemType: itemsModel.get(
                                                                 i).itemType
                                           })
                            }

                            worker.sendMessage({
                                                   action: "updateRecord",
                                                   rid: recordId,
                                                   record: {
                                                       version: version,
                                                       text: recText.text,
                                                       items: items
                                                   },
                                                   login: settings.login,
                                                   pass: settings.password,
                                                   apiURL: settings.apiURL,
                                                   token: mainWindow.token
                                               })
                        }
                    }
                }
            }
        }

        Pane {
            Material.elevation: 4
            Layout.fillHeight: true
            Layout.fillWidth: true

            ColumnLayout {
                anchors.fill: parent

                ListView {
                    id: items
                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    ListModel {
                        id: itemsModel
                    }
                    model: itemsModel

                    delegate: ItemDelegate {
                        width: parent.width
                        text: model.product
                    }
                }

                RowLayout {
                    Label {
                        text: qsTr("Total:")
                        Layout.alignment: Qt.AlignBottom
                    }
                    Label {
                        id: total
                        Layout.alignment: Qt.AlignBottom
                        text: ""
                        font.pixelSize: 16
                        font.bold: true
                    }
                }
            }
        }
    }

    BusyIndicator {
        id: loadingIndicator
        running: isLoading
        anchors.centerIn: parent
    }

    focus: true
    Keys.onReleased: {
        if (event.key === Qt.Key_Back) {
            event.accepted = true
            back()
        }
    }
}
