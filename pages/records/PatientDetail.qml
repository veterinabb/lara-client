import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.1

import "../../common/guiutil.js" as Guiutil

Pane {
    property var patient: ({

                           })
    property bool isLoading: false

    signal back
    signal loadRecord(int recId)
    signal editPatient(var patient)

    function load(patientId) {
        isLoading = true
        worker.sendMessage({
                               action: "getPatient",
                               pid: patientId,
                               model: recordsModel,
                               login: settings.login,
                               pass: settings.password,
                               apiURL: settings.apiURL,
                               token: mainWindow.token
                           })
    }

    function reload() {
        load(patient.id)
    }

    Component.onCompleted: {
        forceActiveFocus() // Required for android back button to work!
    }

    WorkerScript {
        id: worker
        source: "qrc:/api.js"
        onMessage: {
            isLoading = false
            Guiutil.updateConnectionStatus(messageObject)
            if (messageObject.statusCode === 200) {
                patient = messageObject.patient
                editBtn.enabled = true
            }
        }
    }

    ColumnLayout {
        anchors.fill: parent

        Pane {
            Material.elevation: 4
            Layout.fillWidth: true

            GridLayout {
                flow: mainWindow.width
                      > mainWindow.height ? GridLayout.LeftToRight : GridLayout.TopToBottom
                columns: 2 // used only when GridLayout.LeftToRight
                anchors.fill: parent

                RowLayout {
                    Label {
                        text: qsTr("Name:")
                        Layout.alignment: Qt.AlignBottom
                    }
                    Label {
                        id: patientName
                        Layout.alignment: Qt.AlignBottom
                        text: Guiutil.emptyIfUndefined(patient.name)
                        font.pixelSize: 16
                        font.bold: true
                    }
                }

                RowLayout {
                    Label {
                        text: qsTr("Birth date:")
                        Layout.alignment: Qt.AlignBottom
                    }
                    Label {
                        id: birthDate
                        Layout.alignment: Qt.AlignBottom
                        text: new Date(Guiutil.emptyIfUndefined(
                                           patient.birthDate)).toLocaleDateString(
                                  Qt.locale(), Locale.ShortFormat)
                        font.pixelSize: 16
                        font.bold: true
                    }
                }

                RowLayout {
                    Label {
                        text: qsTr("Species:")
                        Layout.alignment: Qt.AlignBottom
                    }
                    Label {
                        id: species
                        Layout.alignment: Qt.AlignBottom
                        text: Guiutil.emptyIfUndefined(patient.species)
                        font.pixelSize: 16
                        font.bold: true
                    }
                }

                RowLayout {
                    Label {
                        text: qsTr("Breed:")
                        Layout.alignment: Qt.AlignBottom
                    }
                    Label {
                        id: breed
                        Layout.alignment: Qt.AlignBottom
                        text: Guiutil.emptyIfUndefined(patient.breed)
                        font.pixelSize: 16
                        font.bold: true
                    }
                }

                RowLayout {
                    Label {
                        text: qsTr("Gender:")
                        Layout.alignment: Qt.AlignBottom
                    }
                    Label {
                        id: gender
                        Layout.alignment: Qt.AlignBottom
                        text: Guiutil.emptyIfUndefined(patient.gender)
                        font.pixelSize: 16
                        font.bold: true
                    }
                }

                RowLayout {
                    Label {
                        text: qsTr("Note:")
                        Layout.alignment: Qt.AlignBottom
                    }
                    Label {
                        id: note
                        Layout.alignment: Qt.AlignBottom
                        text: Guiutil.emptyIfUndefined(patient.note)
                        font.pixelSize: 16
                        font.bold: true
                    }
                }

                RowLayout {
                    Label {
                        text: qsTr("Dead:")
                        Layout.alignment: Qt.AlignBottom
                    }
                    Label {
                        id: dead
                        Layout.alignment: Qt.AlignBottom
                        text: Guiutil.emptyIfUndefined(patient.dead)
                        font.pixelSize: 16
                        font.bold: true
                    }
                }

                RowLayout {
                    Label {
                        text: qsTr("Created:")
                        Layout.alignment: Qt.AlignBottom
                    }
                    Label {
                        id: creator
                        Layout.alignment: Qt.AlignBottom
                        text: Guiutil.formatModifier(patient.creator,
                                                     patient.created)
                        font.pixelSize: 16
                        font.bold: true
                    }
                }

                RowLayout {
                    Label {
                        text: qsTr("Last modified:")
                        Layout.alignment: Qt.AlignBottom
                    }
                    Label {
                        id: modifier
                        Layout.alignment: Qt.AlignBottom
                        text: Guiutil.formatModifier(patient.modifier,
                                                     patient.modified)
                        font.pixelSize: 16
                        font.bold: true
                    }
                }

                RowLayout {
                    Button {
                        text: qsTr("Back")
                        highlighted: true
                        onClicked: back()
                    }
                    Button {
                        id: editBtn
                        text: qsTr("Edit")
                        enabled: false
                        highlighted: true
                        onClicked: editPatient(patient)
                    }
                }
            }
        }

        Pane {
            Material.elevation: 4
            Layout.fillHeight: true
            Layout.fillWidth: true

            ListView {
                id: records
                anchors.fill: parent

                ListModel {
                    id: recordsModel
                }
                model: recordsModel

                delegate: ItemDelegate {
                    width: parent.width
                    text: new Date(model.date).toLocaleString(
                              Qt.locale(),
                              Locale.NarrowFormat) + " - " + model.text
                    onClicked: {
                        loadRecord(model.id)
                    }
                }
            }
        }
    }

    BusyIndicator {
        id: loadingIndicator
        running: isLoading
        anchors.centerIn: parent
    }

    focus: true
    Keys.onReleased: {
        if (event.key === Qt.Key_Back) {
            event.accepted = true
            back()
        }
    }
}
