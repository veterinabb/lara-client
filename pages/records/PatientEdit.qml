import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.1

import "../../common"
import "../../common/guiutil.js" as Guiutil

Flickable {
    property var patient: ({

                           })

    property bool isLoading: false

    signal back
    signal saved

    Component.onCompleted: {
        loadLOVs()
        forceActiveFocus() // Required for android back button to work!
    }

    function loadLOVs() {
        isLoading = true
        worker.sendMessage({
                               action: "getAllGenders",
                               model: genderModel,
                               login: settings.login,
                               pass: settings.password,
                               apiURL: settings.apiURL,
                               token: mainWindow.token
                           })
        worker.sendMessage({
                               action: "getAllSpecies",
                               model: speciesModel,
                               login: settings.login,
                               pass: settings.password,
                               apiURL: settings.apiURL,
                               token: mainWindow.token
                           })
    }

    WorkerScript {
        id: worker
        source: "qrc:/api.js"
        onMessage: {
            isLoading = false
            Guiutil.updateConnectionStatus(messageObject)
            if (messageObject.statusCode === 200) {
                if (messageObject.action === "getAllGenders") {
                    var idx = genderCombo.find(patient.gender)
                    if (idx === -1) {
                        idx = 0
                    }
                    genderCombo.currentIndex = idx
                } else if (messageObject.action === "getAllSpecies") {
                    var idx2 = speciesCombo.find(patient.species)
                    if (idx2 === -1) {
                        idx2 = 0
                    }
                    speciesCombo.currentIndex = idx2
                } else if (messageObject.action === "getAllBreed") {
                    var idx3 = breedCombo.find(patient.breed)
                    if (idx3 === -1) {
                        idx3 = 0
                    }
                    breedCombo.currentIndex = idx3
                } else if (messageObject.action === "updatePatient") {
                    saved() // emmit saved signal
                }
            }
        }
    }

    contentWidth: form.width
    contentHeight: form.height

    ScrollIndicator.vertical: ScrollIndicator {
    }

    ColumnLayout {
        id: form
        spacing: 32

        MaterialTextField {
            id: name
            label: qsTr("Name")
            placeholderText: qsTr("Enter name")
            text: patient.name
            minChars: 1
            maxChars: 100
        }

        RowLayout {
            spacing: 8

            Label {
                text: qsTr("Birth date:")
                Layout.alignment: Qt.AlignBottom
            }

            DatePicker {
                id: birthDate
                date: new Date(patient.birthDate)
                Layout.alignment: Qt.AlignBottom
            }
        }

        MaterialTextField {
            id: note
            label: qsTr("Note")
            placeholderText: qsTr("Enter note")
            text: patient.note
            minChars: 0
            maxChars: 100
        }

        RowLayout {
            Label {
                text: qsTr("Species:")
                Layout.alignment: Qt.AlignBottom
            }
            ComboBox {
                id: speciesCombo
                Layout.alignment: Qt.AlignBottom
                textRole: "name"
                model: ListModel {
                    id: speciesModel
                }
                onCurrentIndexChanged: {
                    worker.sendMessage({
                                           action: "getAllBreed",
                                           model: breedModel,
                                           speciesId: (speciesModel.get(
                                                           speciesCombo.currentIndex)).id,
                                           login: settings.login,
                                           pass: settings.password,
                                           apiURL: settings.apiURL,
                                           token: mainWindow.token
                                       })
                }
            }
        }

        RowLayout {
            Label {
                text: qsTr("Breed:")
                Layout.alignment: Qt.AlignBottom
            }
            ComboBox {
                id: breedCombo
                Layout.alignment: Qt.AlignBottom
                textRole: "name"
                model: ListModel {
                    id: breedModel
                }
            }
        }

        RowLayout {
            Label {
                text: qsTr("Gender:")
                Layout.alignment: Qt.AlignBottom
            }
            ComboBox {
                id: genderCombo
                Layout.alignment: Qt.AlignBottom
                textRole: "name"
                model: ListModel {
                    id: genderModel
                }
            }
        }

        CheckBox {
            id: dead
            checked: patient.dead
            text: qsTr("Dead")
        }

        RowLayout {
            Button {
                text: qsTr("Back")
                highlighted: true
                onClicked: back()
            }

            Button {
                id: saveBtn
                enabled: name.acceptableInput
                text: qsTr("Save")
                highlighted: true
                onClicked: function () {
                    isLoading = true
                    console.log(birthDate.date.toLocaleDateString())
                    console.log(birthDate.date.toJSON())
                    worker.sendMessage({
                                           action: "updatePatient",
                                           pid: patient.id,
                                           patient: {
                                               version: patient.version,
                                               name: name.text,
                                               note: note.text,
                                               dead: dead.checked,
                                               speciesId: (speciesModel.get(
                                                               speciesCombo.currentIndex)).id,
                                               breedId: (breedModel.get(
                                                             breedCombo.currentIndex)).id,
                                               genderId: (genderModel.get(
                                                              genderCombo.currentIndex)).id,
                                               birthDate: birthDate.date.toJSON(
                                                              )
                                           },
                                           login: settings.login,
                                           pass: settings.password,
                                           apiURL: settings.apiURL,
                                           token: mainWindow.token
                                       })
                }
            }
        }
    }


    BusyIndicator {
        id: loadingIndicator
        running: isLoading
        anchors.centerIn: parent
    }

    focus: true
    Keys.onReleased: {
        if (event.key === Qt.Key_Back) {
            event.accepted = true
            back()
        }
    }
}
