import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.1

import "../../common/guiutil.js" as Guiutil

Pane {
    property var owner: ({

                         })
    property bool isLoading: false

    signal back
    signal loadPatient(int pid)
    signal editOwner(var owner)

    function load(oid) {
        isLoading = true
        worker.sendMessage({
                               action: "getOwner",
                               oid: oid,
                               model: patientsModel,
                               login: settings.login,
                               pass: settings.password,
                               apiURL: settings.apiURL,
                               token: mainWindow.token
                           })
    }

    function formatAddress() {
        if (Guiutil.emptyIfUndefined(owner.street) !== "") {
            return owner.street + " " + owner.houseNo + ", " + owner.city
        } else if (Guiutil.emptyIfUndefined(owner.city) !== "") {
            return owner.city + " " + owner.houseNo
        }
        return ""
    }

    Component.onCompleted: {
        forceActiveFocus() // Required for android back button to work!
    }

    WorkerScript {
        id: worker
        source: "qrc:/api.js"
        onMessage: {
            isLoading = false
            Guiutil.updateConnectionStatus(messageObject)
            if (messageObject.statusCode === 200) {
                owner = messageObject.owner
                editBtn.enabled = true
            }
        }
    }

    ColumnLayout {
        anchors.fill: parent

        Pane {
            Material.elevation: 4
            Layout.fillWidth: true

            GridLayout {
                flow: mainWindow.width
                      > mainWindow.height ? GridLayout.LeftToRight : GridLayout.TopToBottom
                columns: 2 // used only when GridLayout.LeftToRight
                anchors.fill: parent

                RowLayout {
                    Label {
                        text: qsTr("Name:")
                        Layout.alignment: Qt.AlignBottom
                    }
                    Label {
                        id: ownerName
                        Layout.alignment: Qt.AlignBottom
                        text: ((Guiutil.emptyIfUndefined(
                                    owner.title) !== "") ? Guiutil.emptyIfUndefined(
                                                               owner.title) + " " : "")
                              + Guiutil.emptyIfUndefined(
                                  owner.firstName) + " " + Guiutil.emptyIfUndefined(
                                  owner.lastName)
                        font.pixelSize: 16
                        font.bold: true
                    }
                }

                RowLayout {
                    Label {
                        text: qsTr("Address:")
                        Layout.alignment: Qt.AlignBottom
                    }
                    Label {
                        id: ownerAddress
                        Layout.alignment: Qt.AlignBottom
                        text: formatAddress()
                        font.pixelSize: 16
                        font.bold: true
                    }
                }

                RowLayout {
                    Label {
                        text: qsTr("Phone 1:")
                        Layout.alignment: Qt.AlignBottom
                    }
                    Label {
                        id: phone1
                        Layout.alignment: Qt.AlignBottom
                        text: Guiutil.emptyIfUndefined(owner.phone1)
                        font.pixelSize: 16
                        font.bold: true
                    }
                }

                RowLayout {
                    Label {
                        text: qsTr("Phone 2:")
                        Layout.alignment: Qt.AlignBottom
                    }
                    Label {
                        id: phone2
                        Layout.alignment: Qt.AlignBottom
                        text: Guiutil.emptyIfUndefined(owner.phone2)
                        font.pixelSize: 16
                        font.bold: true
                    }
                }

                RowLayout {
                    Label {
                        text: qsTr("Email:")
                        Layout.alignment: Qt.AlignBottom
                    }
                    Label {
                        id: email
                        Layout.alignment: Qt.AlignBottom
                        text: Guiutil.emptyIfUndefined(owner.email)
                        font.pixelSize: 16
                        font.bold: true
                    }
                }

                RowLayout {
                    Label {
                        text: qsTr("IC:")
                        Layout.alignment: Qt.AlignBottom
                    }
                    Label {
                        id: ic
                        Layout.alignment: Qt.AlignBottom
                        text: Guiutil.emptyIfUndefined(owner.IC)
                        font.pixelSize: 16
                        font.bold: true
                    }
                }

                RowLayout {
                    Label {
                        text: qsTr("DIC:")
                        Layout.alignment: Qt.AlignBottom
                    }
                    Label {
                        id: dic
                        Layout.alignment: Qt.AlignBottom
                        text: Guiutil.emptyIfUndefined(owner.DIC)
                        font.pixelSize: 16
                        font.bold: true
                    }
                }

                RowLayout {
                    Label {
                        text: qsTr("ICDPH:")
                        Layout.alignment: Qt.AlignBottom
                    }
                    Label {
                        id: icdph
                        Layout.alignment: Qt.AlignBottom
                        text: Guiutil.emptyIfUndefined(owner.ICDPH)
                        font.pixelSize: 16
                        font.bold: true
                    }
                }

                RowLayout {
                    Label {
                        text: qsTr("Note:")
                        Layout.alignment: Qt.AlignBottom
                    }
                    Label {
                        id: note
                        Layout.alignment: Qt.AlignBottom
                        text: Guiutil.emptyIfUndefined(owner.note)
                        font.pixelSize: 16
                        font.bold: true
                    }
                }

                RowLayout {
                    Label {
                        text: qsTr("Created:")
                        Layout.alignment: Qt.AlignBottom
                    }
                    Label {
                        id: creator
                        Layout.alignment: Qt.AlignBottom
                        text: Guiutil.formatModifier(owner.creator, owner.created)
                        font.pixelSize: 16
                        font.bold: true
                    }
                }

                RowLayout {
                    Label {
                        text: qsTr("Last modified:")
                        Layout.alignment: Qt.AlignBottom
                    }
                    Label {
                        id: modifier
                        Layout.alignment: Qt.AlignBottom
                        text: Guiutil.formatModifier(owner.modifier, owner.modified)
                        font.pixelSize: 16
                        font.bold: true
                    }
                }

                RowLayout {
                    Button {
                        text: qsTr("Back")
                        highlighted: true
                        onClicked: back()
                    }
                    Button {
                        id: editBtn
                        text: qsTr("Edit")
                        highlighted: true
                        enabled: false
                        onClicked: editOwner(owner)
                    }
                }
            }
        }

        Pane {
            Material.elevation: 4
            Layout.fillHeight: true
            Layout.fillWidth: true

            ListView {
                id: patients
                anchors.fill: parent

                ListModel {
                    id: patientsModel
                }
                model: patientsModel

                delegate: ItemDelegate {
                    width: parent.width
                    height: 72
                    contentItem: Item {
                        Label {
                            anchors.left: parent.left
                            //anchors.leftMargin: 0 // MD spec: 16dp
                            anchors.verticalCenter: parent.verticalCenter
                            font.family: "Material Icons"
                            font.pixelSize: 48
                            text: "pets"
                        }

                        ColumnLayout {
                            anchors.left: parent.left
                            anchors.leftMargin: 72 // MD spec: 16dp + 72dp
                            anchors.verticalCenter: parent.verticalCenter

                            Label {
                                text: model.name
                                font.bold: true
                                font.pixelSize: 16
                            }

                            Label {
                                text: model.species + ", " + model.breed
                            }
                        }
                    }

                    onClicked: {
                        loadPatient(model.id)
                    }
                }
            }
        }
    }

    BusyIndicator {
        id: loadingIndicator
        running: isLoading
        anchors.centerIn: parent
    }

    focus: true
    Keys.onReleased: {
        if (event.key === Qt.Key_Back) {
            event.accepted = true
            back()
        }
    }
}
