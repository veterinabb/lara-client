import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.1

import "../../common"
import "../../common/guiutil.js" as Guiutil

Flickable {
    property var owner: ({

                         })
    property bool isLoading: false

    signal back
    signal saved

    Component.onCompleted: {
        loadLOVs()
        forceActiveFocus() // Required for android back button to work!
    }

    function loadLOVs() {
        isLoading = true
        worker.sendMessage({
                               action: "getAllTitles",
                               model: titlesModel,
                               login: settings.login,
                               pass: settings.password,
                               apiURL: settings.apiURL,
                               token: mainWindow.token
                           })
    }

    WorkerScript {
        id: worker
        source: "qrc:/api.js"
        onMessage: {
            isLoading = false
            Guiutil.updateConnectionStatus(messageObject)
            if (messageObject.statusCode === 200) {
                if (messageObject.action === "getAllTitles") {
                    var idx = titleCombo.find(owner.title)
                    if (idx === -1) {
                        idx = 0
                    }
                    titleCombo.currentIndex = idx
                } else if (messageObject.action === "updateOwner") {
                    saved() // emmit saved signal
                }
            }
        }
    }

    contentWidth: form.width
    contentHeight: form.height

    ScrollIndicator.vertical: ScrollIndicator {
    }

    ColumnLayout {
        id: form
        spacing: 32

        RowLayout {
            Label {
                text: qsTr("Title:")
                Layout.alignment: Qt.AlignBottom
            }
            ComboBox {
                id: titleCombo
                Layout.alignment: Qt.AlignBottom
                textRole: "name"
                model: ListModel {
                    id: titlesModel
                }
            }
        }

        RowLayout {
            spacing: 24
            MaterialTextField {
                id: firstName
                label: qsTr("First name")
                placeholderText: qsTr("Enter first name")
                text: owner.firstName
                minChars: 0
                maxChars: 50
            }

            MaterialTextField {
                id: lastName
                label: qsTr("Last name")
                placeholderText: qsTr("Enter last name")
                text: owner.lastName
                minChars: 1
                maxChars: 100
            }
        }

        MaterialFilteringSelect {
            id: cityEdit
            searchAction: "searchCity"
            label: qsTr("City")
            text: owner.city
            selectedId: owner.cityId
            onSelectedIdChanged: {
                streetEdit.apiParams = {cityId: cityEdit.selectedId}
            }
            onSelected: {
                streetEdit.text = ""
                streetEdit.selectedId = 0
            }
        }

        MaterialFilteringSelect {
            id: streetEdit
            searchAction: "searchStreet"
            label: qsTr("Street")
            text: owner.street
            selectedId: owner.streetId
        }

        MaterialTextField {
            id: houseNo
            label: qsTr("House No.")
            placeholderText: qsTr("Enter house number")
            text: owner.houseNo
            minChars: 0
            maxChars: 10
        }

        RowLayout {
            spacing: 24
            MaterialTextField {
                id: phone1
                label: qsTr("Phone 1")
                placeholderText: qsTr("Enter phone number")
                text: owner.phone1
                minChars: 0
                maxChars: 20
            }

            MaterialTextField {
                id: phone2
                label: qsTr("Phone 2")
                placeholderText: qsTr("Enter phone number")
                text: owner.phone2
                minChars: 0
                maxChars: 20
            }
        }

        RowLayout {
            spacing: 24
            MaterialTextField {
                id: email
                label: qsTr("Email")
                placeholderText: qsTr("Enter email")
                text: owner.email
                minChars: 0
                maxChars: 100
            }

            MaterialTextField {
                id: note
                label: qsTr("Note")
                placeholderText: qsTr("Enter note")
                text: owner.note
                minChars: 0
                maxChars: 100
            }
        }

        MaterialTextField {
            id: ic
            label: qsTr("IC")
            placeholderText: qsTr("Enter IC")
            text: owner.IC
            minChars: 0
            maxChars: 20
        }

        MaterialTextField {
            id: dic
            label: qsTr("DIC")
            placeholderText: qsTr("Enter DIC")
            text: owner.DIC
            minChars: 0
            maxChars: 20
        }

        MaterialTextField {
            id: icdph
            label: qsTr("ICDPH")
            placeholderText: qsTr("Enter ICDPH")
            text: owner.ICDPH
            minChars: 0
            maxChars: 20
        }

        RowLayout {
            Button {
                text: qsTr("Back")
                highlighted: true
                onClicked: back()
            }

            Button {
                id: saveBtn
                enabled: lastName.acceptableInput
                text: qsTr("Save")
                highlighted: true
                onClicked: function () {
                    isLoading = true
                    worker.sendMessage({
                                           action: "updateOwner",
                                           oid: owner.id,
                                           owner: {
                                               version: owner.version,
                                               firstName: firstName.text,
                                               lastName: lastName.text,
                                               titleId: (titlesModel.get(
                                                             titleCombo.currentIndex)).id,
                                               cityId: cityEdit.selectedId,
                                               streetId: streetEdit.selectedId,
                                               houseNo: houseNo.text,
                                               phone1: phone1.text,
                                               phone2: phone2.text,
                                               email: email.text,
                                               note: note.text,
                                               IC: ic.text,
                                               DIC: dic.text,
                                               ICDPH: icdph.text
                                           },
                                           login: settings.login,
                                           pass: settings.password,
                                           apiURL: settings.apiURL,
                                           token: mainWindow.token
                                       })
                }
            }
        }
    }


    BusyIndicator {
        id: loadingIndicator
        running: isLoading
        anchors.centerIn: parent
    }

    focus: true
    Keys.onReleased: {
        if (event.key === Qt.Key_Back) {
            event.accepted = true
            back()
        }
    }
}
