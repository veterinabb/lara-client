import QtQuick 2.7
import QtQuick.Controls 2.1

StackView {
    signal // pass signal from search field to stackView's item
    search(string text)

    Component.onCompleted: {
        // intial item - search records
        var recs = push("qrc:/pages/records/RecordSearch.qml")
        recs.loadOwner.connect(onLoadOwner)
        search.connect(recs.search)
    }

    function onLoadOwner(oid) {
        var own = push("qrc:/pages/records/OwnerDetail.qml")
        own.load(oid)
        own.back.connect(onBack)
        own.loadPatient.connect(onLoadPatient)
        own.editOwner.connect(onEditOwner)
    }

    function onEditOwner(owner) {
        var own = push("qrc:/pages/records/OwnerEdit.qml", {
                           owner: owner
                       })
        own.back.connect(onBack)
        own.saved.connect(function(){
            pop()
            currentItem.load(owner.id)
            currentItem.forceActiveFocus()
        })
    }

    function onLoadPatient(pid) {
        var patient = push("qrc:/pages/records/PatientDetail.qml")
        patient.load(pid)
        patient.back.connect(onBack)
        patient.loadRecord.connect(onLoadRecord)
        patient.editPatient.connect(onEditPatient)
    }

    function onEditPatient(patient) {
        var own = push("qrc:/pages/records/PatientEdit.qml", {
                           patient: patient
                       })
        own.back.connect(onBack)
        own.saved.connect(function(){
            pop()
            currentItem.load(patient.id)
            currentItem.forceActiveFocus()
        })
    }

    function onLoadRecord(recId) {
        var record = push("qrc:/pages/records/RecordDetail.qml")
        record.load(recId)
        record.back.connect(onBack)
        record.saved.connect(function(){
            pop()
            currentItem.reload()
            currentItem.forceActiveFocus()
        })
    }

    function onBack() {
        pop()
        currentItem.forceActiveFocus()
    }
}
