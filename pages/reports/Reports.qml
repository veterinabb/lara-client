import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3

import "../../common"
import "../../common/guiutil.js" as Guiutil

Pane {

    WorkerScript {
        id: worker
        source: "qrc:/api.js"
        onMessage: {
            busyIndicator.running = false
            Guiutil.updateConnectionStatus(messageObject)
            if (messageObject.statusCode === 200) {
                totalLabel.text = messageObject.records
                incomeLabel.text = Number(messageObject.income).toLocaleCurrencyString(Qt.locale())
                incomeBilledLabel.text = Number(messageObject.incomeBilled).toLocaleCurrencyString(Qt.locale())
                incomeNotBilledLabel.text = Number(messageObject.incomeNotBilled).toLocaleCurrencyString(Qt.locale())
            }
        }
    }

    ColumnLayout {
        spacing: 8
        anchors.fill: parent

        RowLayout {
            spacing: 8

            Label {
                text: qsTr("Date from:")
            }

            DatePicker {
                id: dateFrom
                date: new Date(new Date().getFullYear(), new Date().getMonth(), 1)
                Layout.fillWidth: true
            }
        }

        RowLayout {
            spacing: 8

            Label {
                text: qsTr("Date to:")
            }

            DatePicker {
                id: dateTo
                date: new Date(new Date().getFullYear(), new Date().getMonth()+1, 0)
                endOfDay: true
                Layout.fillWidth: true
            }
        }

        RowLayout {
            spacing: 8

            Button {
                id: runButton
                text: qsTr("run")
                highlighted: true
                Layout.fillWidth: true
                onClicked: {
                    busyIndicator.running = true
                    worker.sendMessage({
                                           action: "incomeReport",
                                           request: {
                                               validFrom: dateFrom.date.toJSON(),
                                               validTo: dateTo.date.toJSON()
                                           },
                                           login: settings.login,
                                           pass: settings.password,
                                           apiURL: settings.apiURL,
                                           token: mainWindow.token
                                       })
                }
            }
        }

        ColumnLayout {
            spacing: 8
            Layout.fillWidth: true
            Layout.fillHeight: true

            RowLayout {
                spacing: 8

                Label {
                    text: qsTr("Total records:")
                }

                Label {
                    id: totalLabel
                    text: "0"
                    font.pixelSize: 16
                    font.bold: true
                }
            }

            RowLayout {
                spacing: 8

                Label {
                    text: qsTr("Income:")
                }

                Label {
                    id: incomeLabel
                    text: ""
                    font.pixelSize: 16
                    font.bold: true
                }
            }

            RowLayout {
                spacing: 8

                Label {
                    text: qsTr("Income (billed):")
                }

                Label {
                    id: incomeBilledLabel
                    text: ""
                    font.pixelSize: 16
                    font.bold: true
                }
            }

            RowLayout {
                spacing: 8

                Label {
                    text: qsTr("Income (not billed):")
                }

                Label {
                    id: incomeNotBilledLabel
                    text: ""
                    font.pixelSize: 16
                    font.bold: true
                }
            }
        }
    }

    BusyIndicator {
        id: busyIndicator
        running: false
        anchors.centerIn: parent
    }
}
