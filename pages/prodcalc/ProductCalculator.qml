import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3

import "../../common/guiutil.js" as Guiutil

Pane {
    signal search(string text)
    Component.onCompleted: {
        search.connect(startSearch)
    }

    function startSearch(text) {
        productsModel.searchRunning = true
        worker.sendMessage({
                               action: "productsearch",
                               model: productsModel,
                               request: {
                                   query: text,
                                   validTo: new Date().toJSON()
                               },
                               login: settings.login,
                               pass: settings.password,
                               apiURL: settings.apiURL,
                               token: mainWindow.token
                           })
    }

    WorkerScript {
        id: worker
        source: "qrc:/api.js"
        onMessage: {
            productsModel.searchRunning = false
            Guiutil.updateConnectionStatus(messageObject)
            if (messageObject.statusCode === 200) {
                if (messageObject.moreItems) {
                    statusToolTip.text = qsTr("More items found")
                } else if (messageObject.total === 0) {
                    statusToolTip.text = qsTr("No items")
                }
                statusToolTip.visible = messageObject.moreItems
                        || messageObject.total === 0
            }
        }
    }

    BusyIndicator {
        id: searchIndicator
        running: productsModel.searchRunning
        anchors.centerIn: parent
    }

    GridLayout {
        id: grid
        anchors.fill: parent
        anchors.margins: 8
        rowSpacing: 8
        columnSpacing: 8
        flow: width > height ? GridLayout.LeftToRight : GridLayout.TopToBottom

        GroupBox {
            title: qsTr("Products (tap for add in)")
            Layout.fillWidth: true
            Layout.fillHeight: true

            ColumnLayout {
                anchors.fill: parent
                ListView {
                    id: products
                    anchors.fill: parent
                    anchors.bottomMargin: 24
                    anchors.topMargin: 24
                    ListModel {
                        id: productsModel
                        property bool searchRunning: false
                    }
                    model: productsModel

                    delegate: ItemDelegate {
                        width: parent.width
                        text: model.name + '<br/>' + model.unit + '/<b>' + Number(
                                  model.price).toLocaleCurrencyString(
                                  Qt.locale()) + '</b>'
                        onClicked: {
                            products.currentIndex = index
                            addDialog.open()
                        }
                    }

                    ScrollIndicator.vertical: ScrollIndicator {
                    }
                }
            }
        }

        GroupBox {
            title: qsTr("Count in (tap for remove)")
            Layout.fillWidth: true
            Layout.fillHeight: true

            ColumnLayout {
                anchors.fill: parent
                ListView {
                    id: selectedProducts
                    anchors.fill: parent
                    anchors.bottomMargin: 24
                    anchors.topMargin: 24
                    ListModel {
                        id: selectedProductsModel
                        function sumPrice() {
                            var ttl = Number(0)
                            for (var i = 0; i < count; i++) {
                                ttl += Number(get(i).price)
                            }
                            return Number(ttl).toLocaleCurrencyString(
                                        Qt.locale())
                        }
                    }
                    model: selectedProductsModel

                    delegate: ItemDelegate {
                        text: model.name + '<br/><b>' + model.price.toLocaleCurrencyString(
                                  Qt.locale()) + '</b>'
                        width: parent.width
                        onClicked: {
                            selectedProducts.currentIndex = index
                            removeDialog.contentItem.text = qsTr(
                                        "Remove %1?").arg(
                                        selectedProductsModel.get(index).name)
                            removeDialog.open()
                        }
                    }

                    ScrollIndicator.vertical: ScrollIndicator {
                    }
                }
            }
        }
        GroupBox {
            title: qsTr("Total")
            Layout.fillWidth: grid.flow === GridLayout.TopToBottom
            Layout.fillHeight: grid.flow === GridLayout.LeftToRight

            Component.onCompleted: {
                total.text = selectedProductsModel.sumPrice()
            }

            RowLayout {
                anchors.fill: parent
                Label {
                    id: total
                    text: ""
                    font.pixelSize: 16
                    font.bold: true
                }
                Item {
                    Layout.fillWidth: true
                }
                Button {
                    id: clearButton
                    text: qsTr("clear")
                    highlighted: true
                    onClicked: {
                        selectedProductsModel.clear()
                        total.text = selectedProductsModel.sumPrice()
                    }
                }
            }
        }
    }

    Dialog {
        id: addDialog
        x: Math.round((mainWindow.width - width) / 2)
        y: Math.round(mainWindow.height / 6)
        width: Math.round(Math.min(mainWindow.width, mainWindow.height) / 3 * 2)
        modal: true
        focus: true
        title: qsTr("Add in")

        standardButtons: Dialog.Ok | Dialog.Cancel
        onAccepted: {
            var pr = Number(productsModel.get(products.currentIndex).price)
            var amt = Number(amountField.text)

            selectedProductsModel.append({
                                             name: productsModel.get(
                                                       products.currentIndex).name,
                                             price: Number(pr * amt)
                                         })
            total.text = selectedProductsModel.sumPrice()
            close()
        }
        onRejected: {
            close()
        }

        contentItem: ColumnLayout {
            id: addDialogColumn
            spacing: 24

            RowLayout {
                spacing: 8

                Label {
                    text: qsTr("Amount:")
                }

                TextField {
                    id: amountField
                    text: "1.0"
                    Layout.fillWidth: true
                }
            }
        }
    }

    Dialog {
        id: removeDialog
        x: Math.round((mainWindow.width - width) / 2)
        y: Math.round(mainWindow.height / 6)
        width: Math.round(Math.min(mainWindow.width, mainWindow.height) / 3 * 2)
        modal: true
        focus: true
        title: qsTr("Remove item")

        standardButtons: Dialog.Ok | Dialog.Cancel
        onAccepted: {
            selectedProductsModel.remove(selectedProducts.currentIndex)
            total.text = selectedProductsModel.sumPrice() + " EUR"
            close()
        }
        onRejected: {
            close()
        }

        contentItem: Label {
            id: removeLabel
            text: "X" // set before open() in caller
            wrapMode: Label.WordWrap
        }
    }
}
