#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QFontDatabase>
#include <QLocale>
#include <QTranslator>
#include <QLibraryInfo>
#include <QSettings>
#include <QFile>
#include <QDebug>
#include <QSslSocket>
//#include <QSslConfiguration>

#define APP_VERSION "1.0.0"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    app.setApplicationName("lara");
    app.setApplicationDisplayName("lara");
    app.setApplicationVersion(APP_VERSION);
    app.setOrganizationDomain("veterinabb.sk");
    app.setOrganizationName("veterinabb");

    // translation
    QLocale::setDefault(QLocale(QLocale::Slovak)); // TODO allow configure in QSettings

    QTranslator sysTrans;
    if (sysTrans.load(QLocale(), QLatin1String("qt"), QLatin1String("_"),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath))) {
           app.installTranslator(&sysTrans);
    }

    QTranslator custTrans;
    if (custTrans.load(QLocale(), QLatin1String("lara"), QLatin1String("_"),
                       QLatin1String(":/"))) {
           app.installTranslator(&custTrans);
    }

    QSettings settings;
    qDebug() << "apiURL: " << settings.value("apiURL").toString();
    qDebug() << "login: " << settings.value("login").toString();
    qDebug() << "password: " << settings.value("password").toString();
    qDebug() << "serverCert: " << settings.value("serverCert").toString();

    /*
     * Load server's TLS certificate.
     * When not found, user default cert bundled with binary
     */
    QFile certFile(settings.value("serverCert", ":/cert.pem").toString());
    bool isOpen = certFile.open(QIODevice::ReadOnly);
    Q_ASSERT(isOpen);
    QSslCertificate cert(&certFile, QSsl::Pem);
    QList<QSslCertificate> certs;
    certs << cert;
    QSslSocket::addDefaultCaCertificates(certs);

    // Ignore SSL Errors entirely
    /*
    QSslConfiguration sslConf = QSslConfiguration::defaultConfiguration();
    sslConf.setPeerVerifyMode(QSslSocket::VerifyNone);
    QSslConfiguration::setDefaultConfiguration(sslConf);
    */

    QFontDatabase fontDatabase;
    if (fontDatabase.addApplicationFont(":/fonts/MaterialIcons-Regular.ttf") == -1) {
        qWarning() << "Failed to load MaterialIcons-Regular.ttf";
    }

    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    engine.rootObjects().first()->setProperty("version", APP_VERSION);

    return app.exec();
}
