function auth(login, pass, apiURL) {
    var token
    var statusCode
    var errorMessage

    var xhr = new XMLHttpRequest()
    var loginUrl = apiURL + '/login'
    console.log('AUTH request POST ' + loginUrl)
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            console.log('AUTH response status: ' + xhr.status)
            console.log('AUTH response: ' + xhr.responseText.toString())
            statusCode = xhr.status
            if (statusCode === 200) {
                token = xhr.responseText.toString()
            } else {
                errorMessage = xhr.responseText.toString()
            }
        }
    }
    xhr.open('POST', loginUrl, false)
    xhr.setRequestHeader('Content-Type', 'application/json')
    xhr.setRequestHeader('Accept', 'text/plain')
    xhr.send(JSON.stringify({
                                username: login,
                                password: pass
                            }))

    return {
        token: token,
        statusCode: statusCode,
        errorMessage: errorMessage
    }
}

function req(verb, apiURL, endpoint, obj, cb, token, jsonResp) {
    var statusCode
    var errorMessage

    console.log('API request: ' + verb + ' ' + apiURL + (endpoint ? '/' + endpoint : ''))
    var xhr = new XMLHttpRequest()
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            console.log('API response status: ' + xhr.status)
            console.log('API response: ' + xhr.responseText.toString())
            statusCode = xhr.status
            var respBody = xhr.responseText.toString()
            if (statusCode === 200 && cb) {
                cb(jsonResp ? JSON.parse(respBody) : respBody, token)
            } else {
                errorMessage = respBody
            }
        }
    }

    xhr.open(verb, apiURL + (endpoint ? '/' + endpoint : ''), false)
    xhr.setRequestHeader('Content-Type', 'application/json')
    xhr.setRequestHeader('Accept', 'application/json')
    if (token) {
        xhr.setRequestHeader('Authorization', 'Bearer ' + token)
    }
    var data = obj ? JSON.stringify(obj) : ''
    xhr.send(data)

    return {
        statusCode: statusCode,
        errorMessage: errorMessage
    }
}

function request(verb, login, pass, apiURL, endpoint, obj, cb, token, jsonResp) {
    var result
    if (!token) {
        result = auth(login, pass, apiURL)
        if (result.token) {
            result = req(verb, apiURL, endpoint, obj, cb,
                         result.token, jsonResp)
        }
    } else {
        result = req(verb, apiURL, endpoint, obj, cb, token, jsonResp)
        if (result.statusCode === 401) {
            result = auth(login, pass, apiURL)
            if (result.token) {
                result = req(verb, apiURL, endpoint, obj, cb,
                             result.token, jsonResp)
            }
        }
    }

    return {
        statusCode: result.statusCode,
        errorMessage: result.errorMessage
    }
}

function processGetAllLOVs(msg, lovType) {
    var result

    msg.model.clear()
    msg.model.sync()
    result = request('GET', msg.login, msg.pass, msg.apiURL,
                     'api/v1/' + lovType , null,
                     function (resp, newToken) {
                         msg.model.append({id:0,name:""}) // no record
                         for (var i = 0; i < resp.items.length; i++) {
                             msg.model.append(resp.items[i])
                         }
                         msg.model.sync()
                         WorkerScript.sendMessage({
                                                      statusCode: 200,
                                                      errorMessage: null,
                                                      newToken: newToken,
                                                      action: msg.action
                                                  })
                     }, msg.token, true)

    if (result.statusCode !== 200) {
        WorkerScript.sendMessage({
                                     statusCode: result.statusCode,
                                     errorMessage: result.errorMessage,
                                     newToken: "",
                                     action: msg.action
                                 })
    }
}

WorkerScript.onMessage = function (msg) {
    var result
    if (msg.action === 'healthcheck') {
        result = req('GET', msg.apiURL, 'ping', null,
                     function (resp, newToken) {
                         if (resp === '.')
                             WorkerScript.sendMessage({
                                                          statusIcon: "signal_cellular_4_bar"
                                                      })
                     }, null, false)

        if (result.statusCode !== 200) {
            WorkerScript.sendMessage({
                                         statusIcon: "signal_cellular_null"
                                     })
        }
    } else if (msg.action === 'search') {
        msg.model.clear()
        msg.model.sync()
        result = request('GET', msg.login, msg.pass, msg.apiURL,
                         'api/v1/search?q=' + msg.request.query, null,
                         function (resp, newToken) {
                             for (var i = 0; i < resp.records.length; i++) {
                                 msg.model.append(resp.records[i])
                             }
                             msg.model.sync()
                             WorkerScript.sendMessage({
                                                          statusCode: 200,
                                                          errorMessage: null,
                                                          newToken: newToken,
                                                          total: resp.total,
                                                          moreItems: resp.total
                                                                     > resp.records.length,
                                                          action: msg.action
                                                      })
                         }, msg.token, true)

        if (result.statusCode !== 200) {
            WorkerScript.sendMessage({
                                         statusCode: result.statusCode,
                                         errorMessage: result.errorMessage,
                                         newToken: "",
                                         action: msg.action
                                     })
        }
    } else if (msg.action === 'getOwner') {
        msg.model.clear()
        msg.model.sync()
        result = request('GET', msg.login, msg.pass, msg.apiURL,
                         'api/v1/owner/' + msg.oid, null,
                         function (resp, newToken) {
                             for (var i = 0; i < resp.patients.length; i++) {
                                 msg.model.append(resp.patients[i])
                             }
                             msg.model.sync()
                             WorkerScript.sendMessage({
                                                          statusCode: 200,
                                                          errorMessage: null,
                                                          newToken: newToken,
                                                          owner: resp,
                                                          action: msg.action
                                                      })
                         }, msg.token, true)

        if (result.statusCode !== 200) {
            WorkerScript.sendMessage({
                                         statusCode: result.statusCode,
                                         errorMessage: result.errorMessage,
                                         newToken: "",
                                         action: msg.action
                                     })
        }
    } else if (msg.action === 'getPatient') {
        msg.model.clear()
        msg.model.sync()
        result = request('GET', msg.login, msg.pass, msg.apiURL,
                         'api/v1/patient/' + msg.pid, null,
                         function (resp, newToken) {
                             for (var i = 0; i < resp.records.length; i++) {
                                 msg.model.append(resp.records[i])
                             }
                             msg.model.sync()
                             WorkerScript.sendMessage({
                                                          statusCode: 200,
                                                          errorMessage: null,
                                                          newToken: newToken,
                                                          patient: resp,
                                                          action: msg.action
                                                      })
                         }, msg.token, true)

        if (result.statusCode !== 200) {
            WorkerScript.sendMessage({
                                         statusCode: result.statusCode,
                                         errorMessage: result.errorMessage,
                                         newToken: "",
                                         action: msg.action
                                     })
        }
    } else if (msg.action === 'getRecord') {
        msg.model.clear()
        msg.model.sync()
        result = request('GET', msg.login, msg.pass, msg.apiURL,
                         'api/v1/record/' + msg.rid, null,
                         function (resp, newToken) {
                             for (var i = 0; i < resp.items.length; i++) {
                                 msg.model.append(resp.items[i])
                             }
                             msg.model.sync()
                             WorkerScript.sendMessage({
                                                          statusCode: 200,
                                                          errorMessage: null,
                                                          newToken: newToken,
                                                          record: resp,
                                                          action: msg.action
                                                      })
                         }, msg.token, true)

        if (result.statusCode !== 200) {
            WorkerScript.sendMessage({
                                         statusCode: result.statusCode,
                                         errorMessage: result.errorMessage,
                                         newToken: "",
                                         action: msg.action
                                     })
        }
    } else if (msg.action === 'getAllTitles') {
        processGetAllLOVs(msg, "title")
    } else if (msg.action === 'getAllGenders') {
        processGetAllLOVs(msg, "gender")
    } else if (msg.action === 'getAllSpecies') {
        processGetAllLOVs(msg, "species")
    }  else if (msg.action === 'getAllBreed') {
        msg.model.clear()
        msg.model.sync()
        result = request('GET', msg.login, msg.pass, msg.apiURL,
                         'api/v1/breed/by-species/' + msg.speciesId, null,
                         function (resp, newToken) {
                             msg.model.append({id:0,name:""}) // not selected
                             for (var i = 0; i < resp.items.length; i++) {
                                 msg.model.append(resp.items[i])
                             }
                             msg.model.sync()
                             WorkerScript.sendMessage({
                                                          statusCode: 200,
                                                          errorMessage: null,
                                                          newToken: newToken,
                                                          action: msg.action
                                                      })
                         }, msg.token, true)

        if (result.statusCode !== 200) {
            WorkerScript.sendMessage({
                                         statusCode: result.statusCode,
                                         errorMessage: result.errorMessage,
                                         newToken: "",
                                         action: msg.action
                                     })
        }
    } else if (msg.action === 'searchCity') {
        msg.model.clear()
        msg.model.sync()
        result = request('GET', msg.login, msg.pass, msg.apiURL,
                         'api/v1/city?q=' + msg.query, null,
                         function (resp, newToken) {
                             msg.model.append({id:0,name:""}) // no city
                             for (var i = 0; i < resp.items.length; i++) {
                                 msg.model.append(resp.items[i])
                             }
                             msg.model.sync()
                             WorkerScript.sendMessage({
                                                          statusCode: 200,
                                                          errorMessage: null,
                                                          newToken: newToken,
                                                          action: msg.action
                                                      })
                         }, msg.token, true)

        if (result.statusCode !== 200) {
            WorkerScript.sendMessage({
                                         statusCode: result.statusCode,
                                         errorMessage: result.errorMessage,
                                         newToken: "",
                                         action: msg.action
                                     })
        }
    } else if (msg.action === 'searchStreet') {
        msg.model.clear()
        msg.model.sync()
        result = request('GET', msg.login, msg.pass, msg.apiURL,
                         'api/v1/street/by-city/' + msg.cityId + '?q=' + msg.query, null,
                         function (resp, newToken) {
                             msg.model.append({id:0,name:""}) // no street
                             for (var i = 0; i < resp.items.length; i++) {
                                 msg.model.append(resp.items[i])
                             }
                             msg.model.sync()
                             WorkerScript.sendMessage({
                                                          statusCode: 200,
                                                          errorMessage: null,
                                                          newToken: newToken,
                                                          action: msg.action
                                                      })
                         }, msg.token, true)

        if (result.statusCode !== 200) {
            WorkerScript.sendMessage({
                                         statusCode: result.statusCode,
                                         errorMessage: result.errorMessage,
                                         newToken: "",
                                         action: msg.action
                                     })
        }
    } else if (msg.action === 'updateOwner') {
        result = request('PUT', msg.login, msg.pass, msg.apiURL,
                         'api/v1/owner/' + msg.oid, msg.owner,
                         function (resp, newToken) {
                             WorkerScript.sendMessage({
                                                          statusCode: 200,
                                                          errorMessage: null,
                                                          newToken: newToken,
                                                          action: msg.action
                                                      })
                         }, msg.token, false)

        if (result.statusCode !== 200) {
            WorkerScript.sendMessage({
                                         statusCode: result.statusCode,
                                         errorMessage: result.errorMessage,
                                         newToken: "",
                                         action: msg.action
                                     })
        }
    } else if (msg.action === 'updatePatient') {
        result = request('PUT', msg.login, msg.pass, msg.apiURL,
                         'api/v1/patient/' + msg.pid, msg.patient,
                         function (resp, newToken) {
                             WorkerScript.sendMessage({
                                                          statusCode: 200,
                                                          errorMessage: null,
                                                          newToken: newToken,
                                                          action: msg.action
                                                      })
                         }, msg.token, false)

        if (result.statusCode !== 200) {
            WorkerScript.sendMessage({
                                         statusCode: result.statusCode,
                                         errorMessage: result.errorMessage,
                                         newToken: "",
                                         action: msg.action
                                     })
        }
    } else if (msg.action === 'updateRecord') {
        result = request('PUT', msg.login, msg.pass, msg.apiURL,
                         'api/v1/record/' + msg.rid, msg.record,
                         function (resp, newToken) {
                             WorkerScript.sendMessage({
                                                          statusCode: 200,
                                                          errorMessage: null,
                                                          newToken: newToken,
                                                          action: msg.action
                                                      })
                         }, msg.token, false)

        if (result.statusCode !== 200) {
            WorkerScript.sendMessage({
                                         statusCode: result.statusCode,
                                         errorMessage: result.errorMessage,
                                         newToken: "",
                                         action: msg.action
                                     })
        }
    } else if (msg.action === 'productsearch') {
        msg.model.clear()
        msg.model.sync()
        result = request('POST', msg.login, msg.pass, msg.apiURL,
                         'api/v1/productsearch', msg.request,
                         function (resp, newToken) {
                             for (var i = 0; i < resp.products.length; i++) {
                                 msg.model.append(resp.products[i])
                             }
                             msg.model.sync()
                             WorkerScript.sendMessage({
                                                          statusCode: 200,
                                                          errorMessage: null,
                                                          newToken: newToken,
                                                          total: resp.total,
                                                          moreItems: resp.total
                                                                     > resp.products.length,
                                                          action: msg.action
                                                      })
                         }, msg.token, true)

        if (result.statusCode !== 200) {
            WorkerScript.sendMessage({
                                         statusCode: result.statusCode,
                                         errorMessage: result.errorMessage,
                                         newToken: "",
                                         action: msg.action
                                     })
        }
    } else if (msg.action === 'incomeReport') {
        result = request('POST', msg.login, msg.pass, msg.apiURL,
                         'api/v1/report/income', msg.request,
                         function (resp, newToken) {
                             WorkerScript.sendMessage({
                                                          statusCode: 200,
                                                          errorMessage: null,
                                                          newToken: newToken,
                                                          income: resp.income,
                                                          records: resp.records,
                                                          incomeBilled: resp.incomeBilled,
                                                          incomeNotBilled: resp.incomeNotBilled,
                                                          action: msg.action
                                                      })
                         }, msg.token, true)

        if (result.statusCode !== 200) {
            WorkerScript.sendMessage({
                                         statusCode: result.statusCode,
                                         errorMessage: result.errorMessage,
                                         newToken: "",
                                         action: msg.action
                                     })
        }
    } else {
        console.log("Unknown API action \"" + msg.action + "\"")
    }
}
